defmodule ElixirJunitTests do
  @moduledoc """
  Documentation for ElixirJunitTests.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirJunitTests.hello()
      :world

  """
  def hello do
    :world
  end

  def answer, do: 666
end
