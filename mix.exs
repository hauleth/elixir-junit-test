defmodule ElixirJunitTests.MixProject do
  use Mix.Project

  def project do
    [
      app: :elixir_junit_tests,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:junit_formatter, ">= 0.0.0", github: "appunite/junit-formatter", branch: "better-message-formatting", only: [:test]}
    ]
  end
end
